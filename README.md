# golang-sandbox

## Initialize

```bash
go mod init gitlab.com/tanooki/issuesdb/golang-sandbox
cat go.mod
touch main.go
```

> `main.go`

```golang
package main

import "fmt"

func main() {
	fmt.Println("👋 Hello World 🌍")
}
```

> run it

```bash
go run main.go 
```

> build it

```
go build
```

Then, you'll get an executable named `golang-sandbox` (the name of the project), you can run it with this command `./golang-sandbox `

```bash
# env GOOS=target-OS GOARCH=target-architecture go build package-import-path
env GOOS=darwin GOARCH=amd64 go build -o golang-sandbox-darwin
env GOOS=linux GOARCH=amd64 go build -o golang-sandbox-linux
```

## Functions

> 🚧 WIP


## Structs

> 🚧 WIP

## Dependencies

> 🚧 WIP


## Refs

- https://hackersandslackers.com/create-your-first-golang-app/
- https://www.wolfe.id.au/2020/03/10/starting-a-go-project/
- https://www.wolfe.id.au/2020/03/10/how-do-i-structure-my-go-project/
- https://golang.org/doc/code
- https://www.digitalocean.com/community/tutorials/how-to-build-go-executables-for-multiple-platforms-on-ubuntu-16-04

- https://gobyexample.com/command-line-flags
- https://www.digitalocean.com/community/tutorials/how-to-use-the-flag-package-in-go
- https://zetcode.com/golang/flag/

### Utile

- https://mholt.github.io/curl-to-go/


