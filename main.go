package main

import (
	"bytes"
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"strings"
)

type configuration struct {
	token  string
	apiUrl string
}

type requestParameters struct {
	id          string // 🤔 are you sure? (int)
	path        string
	body        string
	context     string
	resource    string
	cmd         string
	namespace   string
	name        string
	visibility  string
	description string
	project     string
	group       string
	label       string
	color       string
}

func urlEncodedPath(value string) string {
	replacer := strings.NewReplacer("/", "%2F", ".", "%2E", "-", "%2D", "_", "%5F")
	output := replacer.Replace(value)
	return output
}

func token(token string) string {
	if len(token) > 0 {
		return token
	} else {
		return os.Getenv("GITLAB_TOKEN_ADMIN")
	}
}

/*
func pathOrId(params requestParameters) string {
	if len(params.id) > 0 {
		return params.id
	} else {
		return urlEncodedPath(params.path)
	}
}

func namespaceOrId(params requestParameters) string {
	if len(params.id) > 0 {
		return params.id
	} else {
		return urlEncodedPath(params.namespace)
	}
}

func pathOrName(params requestParameters) string {
	if len(params.path) > 0 {
		return urlEncodedPath(params.path)
	} else {
		return urlEncodedPath(params.name)
	}
}

func nameOrPath(params requestParameters) string {
	if len(params.name) > 0 {
		return urlEncodedPath(params.name)
	} else {
		return urlEncodedPath(params.path)
	}
}
*/

/*
TODO: pager perpage
*/

func jsonToMap(jsonString string) map[string]interface{} {
	var results map[string]interface{}
	// Unmarshal or Decode the JSON to the interface.
	json.Unmarshal([]byte(jsonString), &results)
	return results
}

func getHttpRequest(gitLabConfiguration configuration, url string) (string, error) {
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		fmt.Println(err) // TODO: it's bad
		return "", err
	}
	req.Header.Set("Private-Token", gitLabConfiguration.token)
	req.Header.Set("Content-Type", "application/json")

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		fmt.Println(err) // TODO: it's bad
		return "", err
	}
	defer resp.Body.Close()
	body, _ := ioutil.ReadAll(resp.Body)

	return string(body), nil
}

func postHttpRequest(gitLabConfiguration configuration, url string, bodyData *bytes.Reader) (string, error) {
	req, err := http.NewRequest("POST", url, bodyData)
	if err != nil {
		fmt.Println(err) // TODO: it's bad
		return "", err
	}
	req.Header.Set("Private-Token", gitLabConfiguration.token)
	req.Header.Set("Content-Type", "application/json")

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		fmt.Println(err) // TODO: it's bad
		return "", err
	}
	defer resp.Body.Close()
	body, _ := ioutil.ReadAll(resp.Body)

	return string(body), nil
}

func idFromPath(path string) string {
	return urlEncodedPath(path)
}

func idFromNamespace(namespace string) string {
	return urlEncodedPath(namespace)
}

//TODO:
// - make a pager version
// - make a "all issues" version
// 🙂
func projectIssuesList(gitLabConfiguration configuration, projectPath string) string {
	//go run main.go -path tanooki-workshops/20210616-gitlab-pages/registrations -context projects -resource issues -cmd list

	responseString, err := getHttpRequest(gitLabConfiguration, gitLabConfiguration.apiUrl+"/projects/"+idFromPath(projectPath)+"/issues")
	if err != nil {
		fmt.Println(err) // TODO: it's bad
	}
	return responseString
}

func groupDetails(gitLabConfiguration configuration, groupNamespace string) string {
	// go run main.go -namespace tanooki/issuesdb -context groups -cmd details

	responseString, err := getHttpRequest(gitLabConfiguration, gitLabConfiguration.apiUrl+"/groups/"+idFromNamespace(groupNamespace))
	if err != nil {
		fmt.Println(err) // TODO: it's bad
	}
	return responseString
}

func groupId(gitLabConfiguration configuration, groupNamespace string) int {
	// go run main.go -namespace tanooki/issuesdb -context groups -cmd id
	details := groupDetails(gitLabConfiguration, groupNamespace)
	results := jsonToMap(details)
	return int(results["id"].(float64))
}

// TODO: add members to a project, to a group
func createProject(gitLabConfiguration configuration, projectName string, groupNamespace string, visibility string, description string) string {
	// https://docs.gitlab.com/ee/api/projects.html#create-project
	// go run main.go -project hey -namespace tanooki/issuesdb -context projects -cmd create
	// go run main.go -project hello -namespace tanooki/issuesdb -context projects -cmd create -visibility public -description "my hello database"
	// go run main.go -project hi -namespace tanooki/issuesdb -context projects -cmd create -visibility private -description "my hi database"

	// 🖐 visibility	 / description
	// we need the id of the group
	groupId := groupId(gitLabConfiguration, groupNamespace)

	// if lowercase field private no marshalling
	data := struct {
		Name                   string `json:"name"`
		Path                   string `json:"path"`
		Namespace_id           int    `json:"namespace_id"`
		Initialize_with_readme bool   `json:"initialize_with_readme"`
		Visibility             string `json:"visibility"`
		Description            string `json:"description"`
	}{
		urlEncodedPath(projectName),
		urlEncodedPath(projectName),
		groupId,
		true,
		visibility,
		description}

	jsonDataBytes, err := json.Marshal(data)

	if err != nil {
		// handle err
		fmt.Println(err) //TODO: it's bad
	}
	bodyParams := bytes.NewReader(jsonDataBytes)

	responseString, err := postHttpRequest(gitLabConfiguration, gitLabConfiguration.apiUrl+"/projects", bodyParams)
	if err != nil {
		fmt.Println(err) // TODO: it's bad
	}
	return responseString
}

func createProjectLabel(gitLabConfiguration configuration, projectName string, groupNamespace string, labelName string, labelColor string, labelDescription string) string {

	data := struct {
		Name        string `json:"name"`
		Color       string `json:"color"`
		Description string `json:"description"`
	}{
		labelName,
		labelColor,
		labelDescription}

	jsonDataBytes, err := json.Marshal(data)

	if err != nil {
		// handle err
		fmt.Println(err) //TODO: it's bad
	}
	bodyParams := bytes.NewReader(jsonDataBytes)

	responseString, err := postHttpRequest(gitLabConfiguration, gitLabConfiguration.apiUrl+"/projects/"+urlEncodedPath(groupNamespace+"/"+projectName)+"/labels", bodyParams)
	if err != nil {
		fmt.Println(err) // TODO: it's bad
	}
	return responseString
}

// TODO:
// - issue
// - add label to issue
// - remove label from issue
// - add note to issue
// - search (filter) issue

func main() {

	gitlabInstancePtr := flag.String("gitlab", "https://gitlab.com", "url of the GitLab instance")

	tokenPtr := flag.String("token", "", "GitLab access token") // GITLAB_TOKEN_ADMIN
	// TODO: handle empty token

	idPtr := flag.String("id", "", "project or group(namespace) id")
	pathPtr := flag.String("path", "", "project or group path")
	bodyPtr := flag.String("body", "", "payload (json)")

	namespacePtr := flag.String("namespace", "", "user or group(namespace)")
	namePtr := flag.String("name", "", "value")

	projectPtr := flag.String("project", "", "project name")
	groupPtr := flag.String("group", "", "group name")
	labelPtr := flag.String("label", "", "label name")

	// https://docs.gitlab.com/ee/api/api_resources.html
	contextPtr := flag.String("context", "", "context could be projects, groups or prt of standalone resources")
	// https://docs.gitlab.com/ee/api/api_resources.html#project-resources
	resourcePtr := flag.String("resource", "", "api resource: issues, deployments, ...")

	cmdPtr := flag.String("cmd", "", "list, create, details, ...")

	visibilityPtr := flag.String("visibility", "public", "private, internal, public")
	descriptionPtr := flag.String("description", "", "description of a context or resource")

	colorPtr := flag.String("color", "", "color of a label")

	flag.Parse()
	// TODO: handle the errors

	// Don't forget that we can use project access token too
	// donc avoir un flag token aussi ?
	// see https://docs.gitlab.com/ee/api/#authentication
	gitLabConfiguration := configuration{token(*tokenPtr), *gitlabInstancePtr + "/api/v4"}

	gitLabRequestParameters := requestParameters{
		*idPtr,
		*pathPtr,
		*bodyPtr,
		*contextPtr,
		*resourcePtr,
		*cmdPtr,
		*namespacePtr,
		*namePtr,
		*visibilityPtr,
		*descriptionPtr,
		*projectPtr,
		*groupPtr,
		*labelPtr,
		*colorPtr}

	switch what := gitLabRequestParameters.context + ":" + gitLabRequestParameters.resource + ":" + gitLabRequestParameters.cmd; what {
	case "projects:issues:list":
		//go run main.go -path tanooki-workshops/20210616-gitlab-pages/registrations -context projects -resource issues -cmd list
		fmt.Println(projectIssuesList(gitLabConfiguration, gitLabRequestParameters.path))
	case "projects::create":
		// go run main.go -project ola -namespace tanooki/issuesdb -context projects -cmd create
		// go run main.go -project bonjour -namespace tanooki/issuesdb -context projects -cmd create -visibility public -description "my hello database"
		// go run main.go -project morgen -namespace tanooki/issuesdb -context projects -cmd create -visibility private -description "my hi database"
		fmt.Println(
			createProject(
				gitLabConfiguration,
				gitLabRequestParameters.project,
				gitLabRequestParameters.namespace,
				gitLabRequestParameters.visibility,
				gitLabRequestParameters.description))
	case "groups::details":
		// go run main.go -namespace tanooki/issuesdb -context groups -cmd details
		fmt.Println(groupDetails(gitLabConfiguration, gitLabRequestParameters.namespace))
	case "groups::id":
		// go run main.go -namespace tanooki/issuesdb -context groups -cmd id
		fmt.Println(groupId(gitLabConfiguration, gitLabRequestParameters.namespace))
	case "projects:labels:create":
		// go run main.go -label to-do -color green -description "green label" -namespace tanooki/issuesdb -project hey -context projects -resource labels -cmd create
		fmt.Println(createProjectLabel(
			gitLabConfiguration,
			gitLabRequestParameters.project,
			gitLabRequestParameters.namespace,
			gitLabRequestParameters.label,
			gitLabRequestParameters.color,
			gitLabRequestParameters.description))
	default:
		fmt.Println("🤔 same player shoot again")
	}

}
